- JavaCore 1
  - Theme 1:
    - src/javacore1/theme1basics/Refactoring.java - ✅
    - src/javacore1/theme1basics/Levenstein.java - ✅
  - Theme 2:
    - src/javacore1/theme2/BlockScheme.java - ✅
    - src/javacore1/theme2/Factorial.java - ✅