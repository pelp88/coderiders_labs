package javacore1.theme2;

public class BlockScheme {
    public static void main(String[] args) {
        int a = 3;
        int b = 4;

        if (b > 0) {
            System.out.print(cycle(a, b));
        } else if (b == 0) {
            System.out.print(1);
        } else {
            System.out.print(1 / cycle(a, Math.abs(b)));
        }
    }

    public static int cycle(int a, int b) { // выделенная секция цикла
        int c = 1;
        for (int i = 1; i <= b; i++) {
            c *= a;
        }
        return c;
    }
}
