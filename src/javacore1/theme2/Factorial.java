package javacore1.theme2;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите целое неотрицательное число: ");
        Integer value = null;
        while (value == null) {
            value = tryParse(input.nextLine());
        };

        if (value == 1 | value == 0) {
            System.out.print(1);
        } else if (value < 0) {
            System.out.print("Функция факториала на определена при n < 0");
        } else {
            long time1 = System.nanoTime();
            int result = recurrentCalculation(value);
            time1 = System.nanoTime() - time1;
            System.out.print("Рекурсивно (время выполнения - " + time1 + " н.сек.): " + result + "\n");
            long time2 = System.nanoTime();
            result = cyclicCalculation(value);
            time2 = System.nanoTime() - time2;
            System.out.print("В цикле (время выполнения - " + time2 + " н.сек.): " + result + "\n");
            System.out.print("Быстрее вычислил " + (((time1 < time2) ? "рекурсивный" : "циклический") + " алгоритм"));
        };
    };

    public static int recurrentCalculation(int number) {
        return (number != 1) ? number * recurrentCalculation(number - 1) : 1;
    };

    public static int cyclicCalculation(int number) {
        int result = 1;
        for (int i = 2; i <= number; i++) {
            result *= i;
        };
        return result;
    };

    public static Integer tryParse(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            System.out.print("Введено не целое число, повторите ввод\n");
            return null;
        }
    };
}
