package javacore1.theme1basics;
import java.util.Scanner;

public class Levenstein {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input first word: ");
        String word1 = input.nextLine();
        System.out.print("Input second word: ");
        String word2 = input.nextLine();

        int result = levenstein(word1, word2);
        System.out.print("Result is " + result);
    };

    public static int levenstein(String a, String b){ // алгоритм Фишера
        int len1 = a.length() - 1;
        int len2 = b.length() - 1;

        char[] a_arr = new char[a.length()];
        char[] b_arr = new char[b.length()];

        int[][] difference = new int[len1][len2];

        for (int i = 0; i < len1; i++) {
            difference[i][0] = i;
        }

        for (int j = 0; j < len2; j++) {
            difference[0][j] = j;
        }

        for (int i = 1; i < len1; i++) {
            for (int j = 1; j < len2; j++) {
                int cost;
                if (a_arr[i - 1] == b_arr[j - 1]) {
                    cost = 0;
                } else {
                    cost = 1;
                };

                if (difference[i - 1][j] + 1 < difference[i][j - 1] + 1) {
                    if (difference[i - 1][j] + 1 < difference[i - 1][j - 1] + cost) {
                        difference[i][j] = difference[i - 1][j] + 1;
                    } else {
                        difference[i][j] = Math.min(difference[i - 1][j - 1] + cost, difference[i][j - 1] + 1);
                        ;
                    };
                };
            }
        }
        return difference[len1 - 1][len2 - 1]; // ответом является крайняя нижняя ячейка массива
    }
}
